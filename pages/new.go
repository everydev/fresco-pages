package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func create(f Fresco) string {
	// list objects
	svc := s3.New(session.New())
	from := &s3.ListObjectsInput{
		Bucket: aws.String("fresco-page-template-1"),
		//Prefix: aws.String(""),
		//	MaxKeys: aws.Int64(2),
	}

	result, err := svc.ListObjects(from)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchBucket:
				fmt.Println(s3.ErrCodeNoSuchBucket, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return err.Error()
	}

	var newKey string
	var source string

	extractTitle := title(f.Rest)
	titleSlug := urlslug(extractTitle)

	if titleSlug == "" {
		return "You should pick a title, like /fresco create \"Robbie's Page\""
	}

	for _, item := range result.Contents {

		//newKey = strings.Replace(*item.Key, "test", titleSlug, 1)
		newKey = titleSlug + "/" + *item.Key

		source = *result.Name + "/" + *item.Key

		_, err = svc.CopyObject(&s3.CopyObjectInput{
			Bucket:     aws.String("frsc.pg"),
			Key:        aws.String(newKey),
			CopySource: aws.String(source),
		})
		if err != nil {
			fmt.Printf("Unable to copy item %s : %v", newKey, err)
		}

	}

	return "Check out: https://" + titleSlug + ".fresco.page/"
	// copy objects to new folder
}
