package main

import (
	"regexp"
	"strings"

	"github.com/metal3d/go-slugify"
)

func title(s string) string {
	r := regexp.MustCompile(`"([^}]*)"`)
	matches := r.FindStringSubmatch(s)
	m := strings.ToLower(matches[1])
	return m
}

func urlslug(s string) string {
	slug := slugify.Marshal(s)
	return slug
}
