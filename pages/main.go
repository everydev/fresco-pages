package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Fresco payload
type Fresco struct {
	RawText     string `json:"RawText"`
	TeamDomain  string `json:"TeamDomain"`
	Module      string `json:"Module"`
	Action      string `json:"Action"`
	Rest        string `json:"Rest"`
	UserID      string `json:"UserID"`
	ResponseURL string `json:"ResponseURL"`
}

// Handler for Lambda handling
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	fmt.Println("Received body: ", request.Body)

	f := Fresco{}
	err := json.Unmarshal([]byte(request.Body), &f)
	if err != nil {
		log.Fatal("Unmarshal failed", err)
	}

	var responseText string
	switch f.Action {
	case "create":
		responseText = create(f)
	case "setup":
		responseText = setup(f)
	default:
		responseText = "Try create?"
	}

	return events.APIGatewayProxyResponse{Body: responseText, StatusCode: 200}, nil

}

func main() {

	lambda.Start(Handler)

}
