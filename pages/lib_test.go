package main

import "testing"

func TestTitle(t *testing.T) {
	ti := title("create \"kelly's page\"")
	if ti != "kelly's page" {
		t.Errorf("Got the wrong title: %s", ti)
	}
}
